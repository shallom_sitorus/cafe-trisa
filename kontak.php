<!DOCTYPE html>
<html lang="en">

<head>
    <title>Trisa Cafe</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    
</head>

<body>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
    
    <a class="navbar-brand" href="#">
    <img src="trisa4.png" alt="Logo" style="width: 60px;">
    </a>
    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="About.php">About Us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="kontak.php">Contact</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Lainnya</a>
            </li>
        </ul>
    </div>

</nav>
<div itemscope itemtype="http://schema.org/LocalBusiness">
  <h2><span itemprop="name">Trisa Cafe</span></h2>
  <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
  <span itemprop="streetAddress">Jln.Slamet Riyadi No 71,Surakarta,Jawa Tengah</span>
  Telepon: <span itemprop="telephone">850-648-4200</span>
  Email: <a href=" " itemprop="email">trisacafe@gmail.com</a>
</div>
  

</body>
</html>