-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 08, 2021 at 06:07 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cafe_trisa`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(20) NOT NULL,
  `nama_panjang` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(10) NOT NULL,
  `kategori` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id_member` int(20) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `tanggal_lahir` varchar(50) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `nama_lengkap`, `no_hp`, `tanggal_lahir`, `alamat`, `jenis_kelamin`, `foto`) VALUES
(2, 'salloomm', '22222222222', '2021-06-01', 'nguter', 'Pilihlah Jenis Kelam', '60b9b9ddd6798.jpg'),
(3, 'shalom', '1111111111111111111', '2021-06-01', 'nguter', 'perempuan', '60b9bbcad240c.jpg'),
(4, 'salloomm', '8888888', '2021-06-01', 'aaaa', 'perempuan', '60bddb5ecd98e.jpg'),
(5, 'paksi', '1111111', '2021-06-01', 'indonesia', 'laki-laki', '60bf8f282dac6.png'),
(6, 'rosi', '11111111111', '2021-06-01', 'indonesia', 'perempuan', '60bf916ab8afa.png');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(20) NOT NULL,
  `nama_menu` varchar(20) NOT NULL,
  `harga` int(20) NOT NULL,
  `stok` varchar(10) NOT NULL,
  `kategori` enum('kopi','snack','makanan') NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `nama_menu`, `harga`, `stok`, `kategori`, `gambar`) VALUES
(1, 'Luwak White Kopi', 10000, '23', 'kopi', '60bf6f1deb9f4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan`
--

CREATE TABLE `pesanan` (
  `kode_pesanan` varchar(20) NOT NULL,
  `jumlah_pesanan` varchar(100) NOT NULL,
  `user` int(20) NOT NULL,
  `id_menu` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_anggota`
--

CREATE TABLE `tb_anggota` (
  `id` int(255) NOT NULL,
  `nama_lengkap` char(50) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `tanggal_lahir` varchar(20) NOT NULL,
  `alamat` char(20) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `foto_ktp` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_anggota`
--

INSERT INTO `tb_anggota` (`id`, `nama_lengkap`, `no_hp`, `tanggal_lahir`, `alamat`, `jenis_kelamin`, `foto_ktp`) VALUES
(18, 'Yusuf', '28 Januari 2002', 'laki - laki', 'Langenharjo', '081326565509', '60a5a62013754.png'),
(19, 'Saka', '1 April 2004', 'laki - laki', 'Kali Jambe', '081326565509', '60a5e3c15a9b9.jpeg'),
(22, 'Yusuf Bagus', 'Surakarta, 28 Januari 2002', 'laki - laki', 'Langenharjo', '081326565509', '60b72928ccccc.png'),
(23, 'Yusuf', 'Surakarta, 28 Januari 2002', 'laki - laki', 'Langenharjo', '081326565509', '60b8a12c826d4.jpeg'),
(24, 'salom', 'Surakarta, 28 Januari 2002', 'perempuan', 'nguter', '08999999999', '60b8a40726fee.png');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `kode_transaksi` varchar(100) NOT NULL,
  `jumlah_pesanan` int(100) NOT NULL,
  `total_harga` varchar(100) NOT NULL,
  `kode_pesanan` varchar(100) NOT NULL,
  `id_menu` int(20) NOT NULL,
  `user` int(20) NOT NULL,
  `admin` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(20) NOT NULL,
  `username` varchar(25) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(225) NOT NULL,
  `level` enum('admin','customer') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `email`, `password`, `level`) VALUES
(17, 'shallom', 'shallom@gmail.com', '$2y$10$aIpX3/jkX67E4/KqpNshv.vJ5aGqs7Io4XAIYpvK/wyYX/5V0cvOO', 'admin'),
(18, 'shallomm', 'shallomm@gmail.com', '$2y$10$UXp4FFkwk3msIFocGKLC2.VID3OXbN/y5VQKiy0G/ltJ0gZfAmM/O', 'customer'),
(19, 'sallom', 'sallom@gmail.com', '$2y$10$4WAASda838Tpkr8Dgbc/8eVnlROElgwBiXSqkbbRY6ym5Ay2HqERS', 'customer'),
(20, 'salom', 'salom@gmail.com', '$2y$10$CudahVRRWj.BJPBUTxraLu95MugLh/XJXQccdwmvHvo6R7x65TRoW', 'customer'),
(21, 'saalom', 'saalom@gmail.com', '$2y$10$nrIA7qM9IdJv81VqJAlWIO9vYm/ytO1Hm9DD4KI9opZdoCGAEPFHG', 'customer'),
(22, 'rosai', 'rosai@gmail.com', '$2y$10$Ck5rs.SG/75Untt2hjj7pOt.AGVLcr3unZmO2gqXUJ9jMJ7MDCbJ.', 'customer'),
(23, 'salloom', 'salloom@gmail.com', '$2y$10$i8b4KbN3XfwsaQlC/BctHuvDqOK.cVaau801FEhsAcgJiP/tRvJeq', 'customer'),
(24, 'shalom', 'shalom@gmail.com', '$2y$10$tAD3SG9avb4jYKRmUWEnZO/p7ADPELkKc2bQEMdADIq.rAtdEKhBi', 'customer'),
(25, 'salloomm', 'salloomm@gmail.com', '$2y$10$vBWF7kydEyTSWXnkm703FeHKPROGMMeNTwS/0gcFlv2zxvgkUQp0y', 'customer'),
(26, 'paksi', 'paksi@gmail.com', '$2y$10$knzqsRU/kje/vyxhJpPC/.t8AL/rdXevFkOq.sbENLy5tl8DOIg1O', 'customer'),
(27, 'rosi', 'rosi@gmail.com', '$2y$10$7IlRPz2m5scEdq/IOqS6VeU5JyPZx7yCQIX.qCXQQUzRiQ.6VA8Qy', 'customer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`kode_pesanan`),
  ADD UNIQUE KEY `user` (`user`);

--
-- Indexes for table `tb_anggota`
--
ALTER TABLE `tb_anggota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`kode_transaksi`),
  ADD UNIQUE KEY `user` (`user`),
  ADD UNIQUE KEY `id_menu` (`id_menu`),
  ADD KEY `kode_pesanan` (`kode_pesanan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id_member` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_anggota`
--
ALTER TABLE `tb_anggota`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD CONSTRAINT `pesanan_ibfk_2` FOREIGN KEY (`user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`kode_pesanan`) REFERENCES `pesanan` (`kode_pesanan`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`admin`) REFERENCES `admin` (`id_admin`),
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `transaksi_ibfk_4` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id_menu`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
