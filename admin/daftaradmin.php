<?php



require 'function.php';

if (isset($_POST["submit"])) {

  if (daftar($_POST) > 0) {
    echo "
        <script>
            alert('Registrasi BERHASIL');
            document.location.href = 'login.php';
        </script>
        ";
  } else {
    echo mysqli_error($conn);
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

</head>

<body>
  <nav class="navbar navbar-expand-md bg-dark navbar-dark">


    <a class="navbar-brand" href="#">
      <img src="trisa4.png" alt="Logo" style="width: 60px;">
    </a>

    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="index.php">Home </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="about.php">About Us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="kontak.php">Contact</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="gallery.php">Gallery</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="card">
    <div class="card-header bg-transparent mb-0">
      <h5 class="text-center">Please <span class="font-weight-bold text-primary">SIGN UP</span></h5>
    </div>
    <div class="card-body">
      <form action="" method="post">
        <div class="form-group">
          <label>Nama Lengkap</label>
          <input name="nama_lengkap" type="text" class="form-control" require>
        </div>
        <div class="form-group">
          <label>Username</label>
          <input name="username" type="text" class="form-control" require>
        </div>
        <div class="form-group">
          <label>Email</label>
          <input name="email" type="text" class="form-control" require>
        </div>
        <div class="form-group">
          <label>Password</label>
          <input name="password" type="password" class="form-control" require>
        </div>
        <div class="form-group">
          <label>Konfirmasi Password</label>
          <input name="password2" type="password" class="form-control" require>
        </div>

        <div class="form-group">
          <input type="submit" name="submit" value="Sign Up" class="btn btn-primary btn-block">
        </div>
      </form>
    </div>
  </div>

  <p> Sudah punya akun?
    <a href="login.php">Login di sini</a>
  </p>
  </form>
  <center>
    Copyright 2021 Trisa Cafe
  </center>
  </div>
</body>

</html>