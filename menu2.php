<?php



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Daftar Menu</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Iconic Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="vendors/iconic-fonts/font-awesome/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="vendors/iconic-fonts/flat-icons/flaticon.css">
    <link rel="stylesheet" href="vendors/iconic-fonts/cryptocoins/cryptocoins.css">
    <link rel="stylesheet" href="vendors/iconic-fonts/cryptocoins/cryptocoins-colors.css">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery UI -->
    <link href="assets/css/jquery-ui.min.css" rel="stylesheet">
    <!-- Page Specific CSS (Slick Slider.css) -->
    <link href="assets/css/slick.css" rel="stylesheet">
    <link href="assets/css/datatables.min.css" rel="stylesheet">
    <!-- Costic styles -->
    <link href="assets/css/style.css" rel="stylesheet">
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="32x32" href="favicon.ico">
</head>

<body class="ms-body ms-aside-left-open ms-primary-theme ms-has-quickbar">
    <!-- Preloader -->
    <div id="preloader-wrap">
        <div class="spinner spinner-8">
            <div class="ms-circle1 ms-child"></div>
            <div class="ms-circle2 ms-child"></div>
            <div class="ms-circle3 ms-child"></div>
            <div class="ms-circle4 ms-child"></div>
            <div class="ms-circle5 ms-child"></div>
            <div class="ms-circle6 ms-child"></div>
            <div class="ms-circle7 ms-child"></div>
            <div class="ms-circle8 ms-child"></div>
            <div class="ms-circle9 ms-child"></div>
            <div class="ms-circle10 ms-child"></div>
            <div class="ms-circle11 ms-child"></div>
            <div class="ms-circle12 ms-child"></div>
        </div>
    </div>
    <!-- Overlays -->
    <div class="ms-aside-overlay ms-overlay-left ms-toggler" data-target="#ms-side-nav" data-toggle="slideLeft"></div>
    <div class="ms-aside-overlay ms-overlay-right ms-toggler" data-target="#ms-recent-activity" data-toggle="slideRight"></div>
    <!-- Sidebar Navigation Left -->
    <aside id="ms-side-nav" class="side-nav fixed ms-aside-scrollable ms-aside-left">
        <!-- Logo -->
        <div class="logo-sn ms-d-block-lg">
            <a class="pl-0 ml-0 text-center" href="dashboardadmin.php">
                <img src="trisa4.png" alt="logo">
            </a>
        </div>
        <!-- Navigation -->
        <ul class="accordion ms-main-aside fs-14" id="side-nav-accordion">
            <!-- dashboard -->
            <li class="menu-item">
                <a href="dashboard3.php"> <span><i class="fa fa-tasks fs-16"></i>Halaman Utama</span>
                </a>
            </li>
            <!-- daftar barang -->
            <li class="menu-item">
                <a href="menu2.php"> <span><i class="fa fa-tasks fs-16"></i>Daftar Menu</span>
                </a>
            </li>
            <!-- daftar barang end -->
            <!-- daftar pesanan -->
            <li class="menu-item">
                <a href="keranjang.php"> <span><i class="fas fa-clipboard-list fs-16"></i>Keranjang</span>
                </a>
            </li>
            <!--daftar pesanan end -->


        </ul>
    </aside>


    <!-- Main Content -->
    <main class="body-content">
        <!-- Navigation Bar -->
        <nav class="navbar ms-navbar">
            <div class="ms-aside-toggler ms-toggler pl-0" data-target="#ms-side-nav" data-toggle="slideLeft"> <span class="ms-toggler-bar bg-primary"></span>
                <span class="ms-toggler-bar bg-primary"></span>
                <span class="ms-toggler-bar bg-primary"></span>
            </div>
            <div class="logo-sn logo-sm ms-d-block-sm">
                <a class="pl-0 ml-0 text-center navbar-brand mr-0" href="index.html"><img src="trisa4.png" alt="logo"> </a>
            </div>
            <ul class="ms-nav-list ms-inline mb-0" id="ms-nav-options">
                <li class="ms-nav-item ms-search-form pb-0 py-0">
                    <form class="ms-form" method="post">
                        <div class="ms-form-group my-0 mb-0 has-icon fs-14">
                            <input type="search" class="ms-form-input" name="search" placeholder="Search here..." value=""> <i class="flaticon-search text-disabled"></i>
                        </div>
                    </form>
                </li>


                <li class="ms-nav-item ms-nav-user dropdown">
                    <a href="#" id="userDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="ms-user-img ms-img-round float-right" src="user.png" alt="people">
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right user-dropdown" aria-labelledby="userDropdown">

                        <li class="dropdown-menu-footer">
                            <a class="media fs-14 p-2" href="/trisa_cafe/logout.php"> <span><i class="flaticon-shut-down mr-2"></i> Logout</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="ms-toggler ms-d-block-sm pr-0 ms-nav-toggler" data-toggle="slideDown" data-target="#ms-nav-options"> <span class="ms-toggler-bar bg-primary"></span>
                <span class="ms-toggler-bar bg-primary"></span>
                <span class="ms-toggler-bar bg-primary"></span>
            </div>
        </nav>

        <!-- Quick bar -->
        <aside id="ms-quick-bar" class="ms-quick-bar fixed ms-d-block-lg">

            <ul class="nav nav-tabs ms-quick-bar-list" role="tablist">

                <li class="ms-quick-bar-item ms-has-qa" role="presentation" data-toggle="tooltip" data-placement="left" title="Launch To-do-list" data-title="To-do-list">
                    <a href="#qa-toDo" aria-controls="qa-toDo" role="tab" data-toggle="tab">
                        <i class="flaticon-list"></i>

                    </a>
                </li>
                <li class="ms-quick-bar-item ms-has-qa" role="presentation" data-toggle="tooltip" data-placement="left" title="Launch Reminders" data-title="Reminders">
                    <a href="#qa-reminder" aria-controls="qa-reminder" role="tab" data-toggle="tab">
                        <i class="flaticon-bell"></i>

                    </a>
                </li>
                <li class="ms-quick-bar-item ms-has-qa" role="presentation" data-toggle="tooltip" data-placement="left" title="Launch Notes" data-title="Notes">
                    <a href="#qa-notes" aria-controls="qa-notes" role="tab" data-toggle="tab">
                        <i class="flaticon-pencil"></i>

                    </a>
                </li>
                <li class="ms-quick-bar-item ms-has-qa" role="presentation" data-toggle="tooltip" data-placement="left" title="Invite Members" data-title="Invite Members">
                    <a href="#qa-invite" aria-controls="qa-invite" role="tab" data-toggle="tab">
                        <i class="flaticon-share-1"></i>

                    </a>
                </li>
                <li class="ms-quick-bar-item ms-has-qa" role="presentation" data-toggle="tooltip" data-placement="left" title="Settings" data-title="Settings">
                    <a href="#qa-settings" aria-controls="qa-settings" role="tab" data-toggle="tab">
                        <i class="flaticon-gear"></i>

                    </a>
                </li>
            </ul>
            <div class="ms-configure-qa" data-toggle="tooltip" data-placement="left" title="Configure Quick Access">

                <a href="#">
                    <i class="flaticon-hammer"></i>

                </a>

            </div>

            <!-- Quick bar Content -->
            <div class="ms-quick-bar-content">

                <div class="ms-quick-bar-header clearfix">
                    <h5 class="ms-quick-bar-title float-left">Title</h5>
                    <button type="button" class="close ms-toggler" data-target="#ms-quick-bar" data-toggle="hideQuickBar" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <div class="ms-quick-bar-body tab-content">



                    <div role="tabpanel" class="tab-pane" id="qa-toDo">
                        <div class="ms-quickbar-container ms-todo-list-container ms-scrollable">

                            <form class="ms-add-task-block">
                                <div class="form-group mx-3 mt-0  fs-14 clearfix">
                                    <input type="text" class="form-control fs-14 float-left" id="task-block" name="todo-block" placeholder="Add Task Block" value="">
                                    <button type="submit" class="ms-btn-icon bg-primary float-right"><i class="material-icons text-disabled">add</i></button>
                                </div>
                            </form>

                            <ul class="ms-todo-list">
                                <li class="ms-card ms-qa-card ms-deletable">

                                    <div class="ms-card-header clearfix">
                                        <h6 class="ms-card-title">Task Block Title</h6>
                                        <button data-toggle="tooltip" data-placement="left" title="Add a Task to this block" class="ms-add-task-to-block ms-btn-icon float-right"> <i class="material-icons text-disabled">add</i> </button>
                                    </div>

                                    <div class="ms-card-body">
                                        <ul class="ms-list ms-task-block">
                                            <li class="ms-list-item ms-to-do-task ms-deletable">
                                                <label class="ms-checkbox-wrap ms-todo-complete">
                                                    <input type="checkbox" value="">
                                                    <i class="ms-checkbox-check"></i>
                                                </label>
                                                <span> Task to do </span>
                                                <button type="submit" class="close"><i class="flaticon-trash ms-delete-trigger"> </i></button>
                                            </li>
                                            <li class="ms-list-item ms-to-do-task ms-deletable">
                                                <label class="ms-checkbox-wrap ms-todo-complete">
                                                    <input type="checkbox" value="">
                                                    <i class="ms-checkbox-check"></i>
                                                </label>
                                                <span>Task to do</span>
                                                <button type="submit" class="close"><i class="flaticon-trash ms-delete-trigger"> </i></button>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="ms-card-footer clearfix">
                                        <a href="#" class="text-disabled mr-2"> <i class="flaticon-archive"> </i> Archive </a>
                                        <a href="#" class="text-disabled  ms-delete-trigger float-right"> <i class="flaticon-trash"> </i> Delete </a>
                                    </div>

                                </li>
                            </ul>

                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="qa-reminder">
                        <div class="ms-quickbar-container ms-reminders">

                            <ul class="ms-qa-options">
                                <li> <a href="#" data-toggle="modal" data-target="#reminder-modal"> <i class="flaticon-bell"></i> New Reminder </a> </li>
                            </ul>

                            <div class="ms-quickbar-container ms-scrollable">

                                <div class="ms-card ms-qa-card ms-deletable">
                                    <div class="ms-card-body">
                                        <p> Developer Meeting in Block B </p>
                                        <span class="text-disabled fs-12"><i class="material-icons fs-14">access_time</i> Today - 3:45 pm</span>
                                    </div>
                                    <div class="ms-card-footer clearfix">

                                        <div class="ms-note-editor float-right">
                                            <a href="#" class="text-disabled mr-2" data-toggle="modal" data-target="#reminder-modal"> <i class="flaticon-pencil"> </i> Edit </a>
                                            <a href="#" class="text-disabled  ms-delete-trigger"> <i class="flaticon-trash"> </i> Delete </a>
                                        </div>

                                    </div>
                                </div>
                                <div class="ms-card ms-qa-card ms-deletable">
                                    <div class="ms-card-body">
                                        <p> Start adding change log to version 2 </p>
                                        <span class="text-disabled fs-12"><i class="material-icons fs-14">access_time</i> Tomorrow - 12:00 pm</span>
                                    </div>
                                    <div class="ms-card-footer clearfix">

                                        <div class="ms-note-editor float-right">
                                            <a href="#" class="text-disabled mr-2" data-toggle="modal" data-target="#reminder-modal"> <i class="flaticon-pencil"> </i> Edit </a>
                                            <a href="#" class="text-disabled  ms-delete-trigger"> <i class="flaticon-trash"> </i> Delete </a>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="qa-notes">

                        <ul class="ms-qa-options">
                            <li> <a href="#" data-toggle="modal" data-target="#notes-modal"> <i class="flaticon-sticky-note"></i> New Note </a> </li>
                            <li> <a href="#"> <i class="flaticon-excel"></i> Export to Excel </a> </li>
                        </ul>

                        <div class="ms-quickbar-container ms-scrollable">

                            <div class="ms-card ms-qa-card ms-deletable">
                                <div class="ms-card-header">
                                    <h6 class="ms-card-title">Don't forget to check with the designer</h6>
                                </div>
                                <div class="ms-card-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vulputate urna in faucibus venenatis. Etiam at dapibus neque,
                                        vel varius metus. Pellentesque eget orci malesuada, venenatis magna et
                                    </p>
                                    <ul class="ms-note-members clearfix mb-0">
                                        <li class="ms-deletable"> <img src="assets/img/people/people-3.jpg" alt="member"> </li>
                                        <li class="ms-deletable"> <img src="assets/img/people/people-5.jpg" alt="member"> </li>
                                    </ul>
                                </div>
                                <div class="ms-card-footer clearfix">

                                    <div class="dropdown float-left">
                                        <a href="#" class="text-disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon-share-1"></i> Share
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="dropdown-menu-header">
                                                <h6 class="dropdown-header ms-inline m-0"><span class="text-disabled">Share With</span></h6>
                                            </li>
                                            <li class="dropdown-divider"></li>
                                            <li class="ms-scrollable ms-dropdown-list ms-members-list">
                                                <a class="media p-2" href="#">
                                                    <div class="mr-2 align-self-center">
                                                        <img src="assets/img/people/people-10.jpg" class="ms-img-round" alt="people">
                                                    </div>
                                                    <div class="media-body">
                                                        <span>John Doe</span>
                                                    </div>
                                                </a>
                                                <a class="media p-2" href="#">
                                                    <div class="mr-2 align-self-center">
                                                        <img src="assets/img/people/people-9.jpg" class="ms-img-round" alt="people">
                                                    </div>
                                                    <div class="media-body">
                                                        <span>Raymart Sandiago</span>
                                                    </div>
                                                </a>
                                                <a class="media p-2" href="#">
                                                    <div class="mr-2 align-self-center">
                                                        <img src="assets/img/people/people-7.jpg" class="ms-img-round" alt="people">
                                                    </div>
                                                    <div class="media-body">
                                                        <span>Heather Brown</span>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="ms-note-editor float-right">
                                        <a href="#" class="text-disabled mr-2" data-toggle="modal" data-target="#notes-modal"> <i class="flaticon-pencil"> </i> Edit </a>
                                        <a href="#" class="text-disabled  ms-delete-trigger"> <i class="flaticon-trash"> </i> Delete </a>
                                    </div>

                                </div>
                            </div>

                            <div class="ms-card ms-qa-card ms-deletable">
                                <div class="ms-card-header">
                                    <h6 class="ms-card-title">Perform the required unit tests</h6>
                                </div>
                                <div class="ms-card-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vulputate urna in faucibus venenatis. Etiam at dapibus neque,
                                        vel varius metus. Pellentesque eget orci malesuada, venenatis magna et
                                    </p>
                                    <ul class="ms-note-members clearfix mb-0">
                                        <li class="ms-deletable"> <img src="assets/img/people/people-9.jpg" alt="member"> </li>
                                    </ul>
                                </div>
                                <div class="ms-card-footer clearfix">

                                    <div class="dropdown float-left">
                                        <a href="#" class="text-disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon-share-1"></i> Share
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="dropdown-menu-header">
                                                <h6 class="dropdown-header ms-inline m-0"><span class="text-disabled">Share With</span></h6>
                                            </li>
                                            <li class="dropdown-divider"></li>
                                            <li class="ms-scrollable ms-dropdown-list ms-members-list">
                                                <a class="media p-2" href="#">
                                                    <div class="mr-2 align-self-center">
                                                        <img src="assets/img/people/people-10.jpg" class="ms-img-round" alt="people">
                                                    </div>
                                                    <div class="media-body">
                                                        <span>John Doe</span>
                                                    </div>
                                                </a>
                                                <a class="media p-2" href="#">
                                                    <div class="mr-2 align-self-center">
                                                        <img src="assets/img/people/people-9.jpg" class="ms-img-round" alt="people">
                                                    </div>
                                                    <div class="media-body">
                                                        <span>Raymart Sandiago</span>
                                                    </div>
                                                </a>
                                                <a class="media p-2" href="#">
                                                    <div class="mr-2 align-self-center">
                                                        <img src="assets/img/people/people-7.jpg" class="ms-img-round" alt="people">
                                                    </div>
                                                    <div class="media-body">
                                                        <span>Heather Brown</span>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="ms-note-editor float-right">
                                        <a href="#" class="text-disabled mr-2" data-toggle="modal" data-target="#notes-modal"> <i class="flaticon-pencil"> </i> Edit </a>
                                        <a href="#" class="text-disabled  ms-delete-trigger"> <i class="flaticon-trash"> </i> Delete </a>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="qa-invite">

                        <div class="ms-quickbar-container text-center ms-invite-member">
                            <i class="flaticon-network"></i>
                            <p>Invite Team Members</p>
                            <form>
                                <div class="ms-form-group">
                                    <input type="text" placeholder="Member Email" class="form-control" name="invite-email" value="">
                                </div>
                                <div class="ms-form-group">
                                    <button type="submit" name="invite-member" class="btn btn-primary w-100">Invite</button>
                                </div>
                            </form>
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="qa-settings">

                        <div class="ms-quickbar-container text-center ms-invite-member">
                            <div class="row">
                                <div class="col-md-12 text-left mb-5">
                                    <h4 class="section-title bold">Customize</h4>
                                    <div>
                                        <label class="ms-switch">
                                            <input type="checkbox" id="dark-mode">
                                            <span class="ms-switch-slider round"></span>
                                        </label>
                                        <span> Dark Mode </span>
                                    </div>
                                    <div>
                                        <label class="ms-switch">
                                            <input type="checkbox" id="remove-quickbar">
                                            <span class="ms-switch-slider round"></span>
                                        </label>
                                        <span> Remove Quickbar </span>
                                    </div>
                                </div>
                                <div class="col-md-12 text-left">
                                    <h4 class="section-title bold">Keyboard Shortcuts</h4>
                                    <p class="ms-directions mb-0"><code>Esc</code> Close Quick Bar</p>
                                    <p class="ms-directions mb-0"><code>Alt + (1 -&gt; 6)</code> Open Quick Bar Tab</p>
                                    <p class="ms-directions mb-0"><code>Alt + Q</code> Enable Quick Bar Configure Mode</p>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </aside>
        <!-- Reminder Modal -->
        <div class="modal fade" id="reminder-modal" tabindex="-1" role="dialog" aria-labelledby="reminder-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-secondary">
                        <h5 class="modal-title has-icon text-white"> New Reminder</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form>
                        <div class="modal-body">
                            <div class="ms-form-group">
                                <label>Remind me about</label>
                                <textarea class="form-control" name="reminder"></textarea>
                            </div>
                            <div class="ms-form-group"> <span class="ms-option-name fs-14">Repeat Daily</span>
                                <label class="ms-switch float-right">
                                    <input type="checkbox"> <span class="ms-switch-slider round"></span>
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="ms-form-group">
                                        <input type="text" class="form-control datepicker" name="reminder-date" value="" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="ms-form-group">
                                        <select class="form-control" name="reminder-time">
                                            <option value="">12:00 pm</option>
                                            <option value="">1:00 pm</option>
                                            <option value="">2:00 pm</option>
                                            <option value="">3:00 pm</option>
                                            <option value="">4:00 pm</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-secondary shadow-none" data-dismiss="modal">Add Reminder</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Notes Modal -->
        <div class="modal fade" id="notes-modal" tabindex="-1" role="dialog" aria-labelledby="notes-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-secondary">
                        <h5 class="modal-title has-icon text-white" id="NoteModal">New Note</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form>
                        <div class="modal-body">
                            <div class="ms-form-group">
                                <label>Note Title</label>
                                <input type="text" class="form-control" name="note-title" value="">
                            </div>
                            <div class="ms-form-group">
                                <label>Note Description</label>
                                <textarea class="form-control" name="note-description"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-secondary shadow-none" data-dismiss="modal">Add Note</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- SCRIPTS -->
        <!-- Global Required Scripts Start -->
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/perfect-scrollbar.js">
        </script>
        <script src="assets/js/jquery-ui.min.js">
        </script>
        <!-- Global Required Scripts End -->
        <!-- Page Specific Scripts Start -->

        <script src="assets/js/Chart.bundle.min.js">
        </script>
        <script src="assets/js/widgets.js"> </script>
        <script src="assets/js/clients.js"> </script>
        <script src="assets/js/Chart.Financial.js"> </script>
        <script src="assets/js/d3.v3.min.js">
        </script>
        <script src="assets/js/topojson.v1.min.js">
        </script>
        <script src="assets/js/datatables.min.js">
        </script>
        <script src="assets/js/data-tables.js">
        </script>
        <!-- Page Specific Scripts Finish -->
        <!-- Costic core JavaScript -->
        <script src="assets/js/framework.js"></script>
        <!-- Settings -->
        <script src="assets/js/settings.js"></script>
        <section class="konten">
        <div class="container">
        <h4 class="text-center font-weight-bold m-4">PRODUK PILIHAN</h4>
            <div class="row">

                <?php 
                    include 'function.php';
                     $makan = mysqli_query($conn, "SELECT * FROM menu ORDER BY id_menu DESC");
                     if(mysqli_num_rows($makan)>0){
                     while($row = mysqli_fetch_array( $makan)){
                ?>
                <div class="card-body bg-light pb-0">
                    <h5 class="card-title"><?php echo $row['nama_menu']?></h5>
                    <div class="card col-md-3  pt-3 z-index">
                    <img src="../trisa_cafe/img/<?php echo $row['gambar']?>" class="card-img-top pb-1"
                        alt="..." width="70%">
                    <i class="fas fa-star"></i>
                    <p class="card-text"><?php echo $row['deskripsi']?></p>
                </div>
                        <!-- div card footer -->
                <div class="card-footer bg-light border-0 pb-3">
                <a href="pesan.php?id=<?php echo $row['id_menu'];?>" class="btn btn-success rounded-pill">Buy</a>
                <!-- <a href="#" class="btn btn-primary rounded-pill" data-bs-target="#produk<?php echo $row['id_menu']?>" data-bs-toggle="modal">Detail</a> -->
                    <a href="#" class="btn btn-success rounded-pill">Rp.
                        <?php echo number_format($row['harga']) ?></a>
                </div>
                <?php } }else{?>
                <table>
                <tr>
                    <td colspan="8">Tidak ada data</td>
                </tr>
                </table>

                <?php } ?>
            </div>
            </div>
            </section>

</body>

</html>