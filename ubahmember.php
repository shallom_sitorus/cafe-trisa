<?php

require 'function.php';

$id = $_GET["id"];

$rows = query("SELECT * FROM member WHERE id_member = $id")[0];

if (isset($_POST["submit"])) {

    if (ubahmember($_POST) > 0) {
        echo "
        <script>
            alert('Data BERHASIL diubah');
            document.location.href = 'member.php';
        </script>
        ";
    } else {
        echo "
        <script>
        alert('Data GAGAL diubah!');
        document.location.href = 'member.php';
        </script>

    ";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Data Member</title>
</head>

<body>
<a href="member.php">kembali</a>
    <h1>Ubah Data Member</h1>
    <form action="" method="post" enctype="multipart/form-data">
        <br>
        <input type="hidden" name="id" value="<?= $rows['id_member']; ?>">
        <input type="hidden" name="gambarlama" value="<?= $rows['foto']; ?>">
        <br>
        <label>Nama Lengkap</label>
        <br>
        <input name="nama_lengkap" type="text" value="<?= $rows['nama_lengkap']; ?>" require>
        <br>
        <label>No Hp</label>
        <br>
        <input name="no_hp" type="text" value="<?= $rows['no_hp']; ?>" require>
        <br>
        <label>Tanggal Lahir</label>
        <br>
        <input name="tanggal_lahir" type="date" value="<?= $rows['tanggal_lahir']; ?>" require>
        <br>
        <label>Alamat</label>
        <br>
        <input name="alamat" type="text" value="<?= $rows['alamat']; ?>" require>
        <br>
        <label>Jenis Kelamin</label>
        <br>
        <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="jenis_kelamin" value="<?= $row['jenis_kelamin']; ?>" require>
            <option selected>Pilihlah Jenis Kelamin</option>
            <option value="laki-laki">Laki-laki</option>
            <option value="perempuan">Perempuan</option>
        </select>
        <br>
        <label> Foto </label>
        <br>
        <img src="img/<?= $rows['foto']; ?>" width="40">
        <input type="file" name="foto" id="foto">
        <br>
        <button type="submit" name="submit">Ubah</button>
        <br>
    </form>
</body>

</html>