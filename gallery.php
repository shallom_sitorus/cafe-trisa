<!DOCTYPE html>
<html lang="en">

<head>
    <title>Trisa Cafe</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

</head>

<body>
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">

        <a class="navbar-brand" href="#">
            <img src="trisa4.png" alt="Logo" style="width: 60px;">
        </a>
        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Navbar links -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Home </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="About.php">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="kontak.php">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="gallery.php">Gallery</a>
                </li>
            </ul>
        </div>
    </nav>
    <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active" data-interval="10000">
                <img src="kopi1.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="kopi2.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="kopi3.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="kopi4.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="kopi5.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="kopi6.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="kopi7.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="kopi8.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="kopi9.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="kopi10.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="kopi11.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="kopi12.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="kopi13.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="kopi14.jpg" class="d-block w-100" alt="...">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>


</body>

</html>