<!DOCTYPE html>
<html lang="en">

<head>
    <title>Trisa Cafe</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    
</head>

<body>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
    
    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="about.php">About Us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="kontak.php">Contact</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Gallery</a>
            </li>
        </ul>
    </div>
</nav>

<div class="jumbotron text-center">
    <img src="trisa4.png"  class="rounded float-center" style="width:300px ;">
   
    <h4><i>"I'd much rather hang out in a cafe. That's where things are really happening."</i></h4>

   
    <a class="btn btn-success" href="login.php" role="button">Login</a>
    <a class="btn btn-primary" href="daftar.php" role="button">Sign Up</a>
    

<div class="jumbotron text-center" style="margin: bottom 0 px;">
    <p>Copyright 2021 Trisa Cafe</p>
</div>
</body>
</html> 