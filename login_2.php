<?php
 session_start();
 if(isset($_SESSION["login"])){
    header("Location: dashboard.php");
    exit;

}
require 'function.php';
if (isset($_POST["login"])){
    
    global $conn;
    $username = $_POST["username"];
    $password = $_POST["password"];

    $result = mysqli_query($conn, "SELECT * FROM user WHERE username = '$username'");

    if (mysqli_num_rows($result)){
        
        $rows = mysqli_fetch_assoc($result);
       
        if (password_verify($password, $rows["password"])){
            $_SESSION["login"] = true;
          
            header("Location: dashboard.php");
            exit;
    
}
    }

    $error = true;

}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  

</head>

<body>
  <nav class="navbar navbar-expand-md bg-dark navbar-dark">


    <a class="navbar-brand" href="#">
      <img src="trisa4.png" alt="Logo" style="width: 60px;">
    </a>

    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="index.php">Home </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="about.php">About Us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="kontak.php">Contact</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Gallery</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container">

    <?php if (isset($error)) : ?>
      <p>username / password salah</p>
    <?php endif ?>

    <div class="card">
      <div class="card-header bg-transparent mb-0">
        <h5 class="text-center">Please <span class="font-weight-bold text-primary">LOGIN</span></h5>
      </div>
      <div class="card-body">
        <form action="" method="post">
          <div class="form-group">
            <input type="text" name="username" class="form-control" placeholder="Username">
          </div>
          <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="password">
          </div>
          <div class="form-label-group">
            <select class="form-control" name="level">
              <option value="admin">Admin</option>
              <!-- <option value="kasir">Kasir</option> -->
              <option value="customer">Customer</option>
            </select>
          </div>
          <br>
          <div class="form-group custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
            <label class="custom-control-label" for="customControlAutosizing">Remember me</label>
          </div>
          <div class="form-group">
          <button type="submit" name="login" class="btn btn-primary btn-block">Login</button>
          </div>
        </form>
      </div>
    </div>

    <p> Belum punya akun?
      <a href="daftar.php">Daftar di sini</a>
    </p>
    </form>
    <center>
      Copyright 2021-<?= date('Y') ?> | Trisa Cafe
    </center>
  </div>
</body>
</html>
