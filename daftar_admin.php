<?php



require 'function.php';

if (isset($_POST["submit"])) {

  if (daftar($_POST) > 0) {
    echo "
        <script>
            alert('Registrasi BERHASIL');
            document.location.href = 'login.php';
        </script>
        ";
  } else {
    echo mysqli_error($conn);
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>

<body>
  <nav class="navbar navbar-expand-md bg-dark navbar-dark">


    <a class="navbar-brand" href="#">
      <img src="trisa4.png" alt="Logo" style="width: 60px;">
    </a>

    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="index.php">Home </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="about.php">About Us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="kontak.php">Contact</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Gallery</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container">
    <div class="card">
      <div class="card-header bg-transparent mb-0">
        <h5 class="text-center"><span class="font-weight-bold text-primary">Daftar Admin</span></h5>
      </div>
      <div class="card-body">
        <form action="" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <input type="text" name="nama_lengkap" class="form-control" placeholder="Nama Lengkap">
          </div>
          <div class="form-group">
            <input type="text" name="username" class="form-control" placeholder="Username">
          </div>
          <div class="form-group">
            <input type="text" name="email" class="form-control" placeholder="Email">
          </div>
          <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Password">
          </div>
          <div class="form-group">
            <input type="password" name="password2" class="form-control" placeholder=" Konfirmasi Password">
          </div>
          <div class="form-label-group">
            <select class="form-control" name="level">
              <option selected>Open this select menu</option>
              <!-- <option value="kasir">Kasir</option> -->
              <option value="admin">Admin</option>
            </select>
          </div>
          <br>
          <input type="submit" name="submit" value="Daftar" class="btn btn-primary btn-block">
      </div>
      </form>
    </div>
  </div>


  </form>
  <center>
    Copyright 2021-<?= date('Y') ?> | Trisa Cafe
  </center>
  </div>
</body>
<!-- <body>
  <div class="container">
    <h1>Halaman Daftar</h1>
    <form action="" method="post">
      <label>Nama Lengkap</label>
      <br>
      <input name="nama_lengkap" type="text" require>
      <br>
      <label>Username</label>
      <br>
      <input name="username" type="text" require>
      <br>
      <label>Email</label>
      <br>
      <input name="email" type="text" require>
      <br>
      <label>Password</label>
      <br>
      <input name="password" type="password" require>
      <br>
      <br>
      <label>Konfirmasi Password</label>
      <br>
      <input name="password2" type="password" require>
      <br>
      <label>Level</label>
      <br>
      <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="level">
        <option selected>Open this select menu</option>
        <!-- <option value="customer">Customer</option> -->
<!-- <option value="admin">Admin</option> -->
<!-- <option value="3">Three</option> -->
<!-- </select>
<br>
<button type="submit" name="submit">Daftar</button>
<p> Sudah punya akun?
  <a href="login.php">Login di sini</a>
</p>
<br>
<h5> @2021 | Trisa Cafe </h5> </br>
</form>
</div>
</body>  -->

</html>