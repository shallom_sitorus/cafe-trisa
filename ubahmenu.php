<?php

require 'function.php';

$id = $_GET["id"];

$rows = query("SELECT * FROM menu WHERE id_menu = $id")[0];

if (isset($_POST["submit"])) {

    if (ubahmenu($_POST) > 0) {
        echo "
        <script>
            alert('Data BERHASIL diubah');
            document.location.href = 'menu.php';
        </script>
        ";
    } else {
        echo "
        <script>
        alert('Data GAGAL diubah!');
        document.location.href = 'menu.php';
        </script>

    ";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Data Menu</title>
</head>

<body>
<a href="menu.php">kembali</a>
    <h1>Ubah Data Menu</h1>
    <form action="" method="post" enctype="multipart/form-data">
        <br>
        <input type="hidden" name="id" value="<?= $rows['id_menu']; ?>">
        <input type="hidden" name="gambarlama" value="<?= $rows['gambar']; ?>">
        <br>
        <label>Nama Menu</label>
        <br>
        <input name="nama_menu" type="text" value="<?= $rows['nama_menu']; ?>" require>
        <br>
        <label>Harga</label>
        <br>
        <input name="harga" type="text" value="<?= $rows['harga']; ?>" require>
        <br>
        <label>Stok</label>
        <br>
        <input name="stok" type="text" value="<?= $rows['stok']; ?>" require>
        <br>
        <label>Kategori</label>
        <br>
        <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="kategori" value="<?= $row['kategori']; ?>" require>
        <option selected>Kategori</option>
        <option value="kopi">Kopi</option>
        <option value="snack">Snack</option>
        <option value="makanan">Makanan</option>
        </select>
        <br>
        <label>Deskripsi</label>
        <br>
        <input name="deskripsi" type="text" value="<?= $rows['deskripsi']; ?>" require>
        <br>
        <label>Gambar</label>
        <br>
        <img src="img/<?= $rows['gambar']; ?>" width="40">
        <input type="file" name="gambar" id="gambar">
        <br>
        <button type="submit" name="submit">Ubah</button>
        <br>
    </form>
</body>

</html>