<?php


require 'function.php';

if (isset($_POST["submit"])) {

  if (tambahmenu($_POST) > 0) {
    echo "
        <script>
            alert('Tambah Menu BERHASIL');
            document.location.href = 'menu_admin.php';
        </script>
        ";
  } else {
    echo mysqli_error($conn);
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>

<body>
  <div class="contrainer">
    <div class="card col-md-6">
      <div class="card-header bg-transparent mb-0">
        <h5 class="text-center"><span class="font-weight-bold text-primary">Tambah Menu</span></h5>
      </div>
      <div class="card-body">
        <form action="" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <input type="text" name="nama_menu" class="form-control" placeholder="Nama Menu">
          </div>
          <div class="form-group">
            <input type="text" name="harga" class="form-control" placeholder="Harga">
          </div>
          <div class="form-group">
            <input type="text" name="stok" class="form-control" placeholder="Stok">
          </div>
          <div class="form-label-group">
            <select class="form-control" name="kategori">
              <option selected>Kategori</option>
              <!-- <option value="kasir">Kasir</option> -->
              <option value="kopi">Kopi</option>
              <option value="snack">Snack</option>
              <option value="makanan">Makanan</option>
            </select>
          </div>
          <br>
          <div class="form-group">
            <input type="text" name="deskripsi" class="form-control" placeholder="Deskripsi">
          </div>
          <br>
          <label>Gambar</label>
          <br>
          <input type="file" name="gambar" required>
          <br>
          <br>
          <input type="submit" name="submit" value="Tambah" class="btn btn-primary btn-block">
      </div>
      </form>
    </div>
  </div>

  
</body>

</html>