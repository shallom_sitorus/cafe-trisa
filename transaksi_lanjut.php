<?php

$conn = mysqli_connect('localhost', 'root', '', 'cafe_trisa');
session_start();
require 'function.php';
// include 'keranjang.php';


if (isset($_POST["submit"])) {

    if (transaksi_lanjut($_POST) > 0) {
        echo "
        <script>
            alert('Silahkan Menunggu Pesanan Tiba');
            document.location.href = 'akhir.php';
        </script>
        ";
    } else {
        echo mysqli_error($conn);
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>

<body>
    <section class="konten">
        <div class="container">
            <div class="card mt-5">
                <div class="card-body">
                    <h3 class="display-4">Keranjang Belanja</h3>
                    <hr>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Menu</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $nomor = 1; ?>
                            <?php $totalbelanja = 0; ?>
                            <?php foreach ($_SESSION["keranjang"] as $id_menu => $jumlah) : ?>
                                <!-- menampilkan produk yang sedang diperulangkan berdasarkan id produk -->
                                <?php
                                $ambil = $conn->query("SELECT * FROM menu WHERE id_menu='$id_menu'");
                                $pecah = $ambil->fetch_assoc();
                                $subharga = $pecah["harga"] * $jumlah;
                                // echo "<pre>";
                                // print_r($pecah);
                                // echo "</pre>";
                                ?>
                                <tr>
                                    <td><?php echo $nomor; ?></td>
                                    <td><?php echo $pecah["nama_menu"] ?></td>
                                    <td>Rp. <?php echo number_format($pecah["harga"]); ?></td>
                                    <td><?php echo $jumlah; ?></td>
                                    <td>Rp. <?php echo number_format($subharga); ?></td>
                                </tr>
                                <?php $nomor++; ?>
                                <?php $totalbelanja += $subharga; ?>
                            <?php endforeach ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4">Total Belanja</th>
                                <th>Rp.<?php echo number_format($totalbelanja) ?></th>
                                <?php function transaksi_lanjut($data)
                                {

                                    global $conn;
                                    //ambil data dari tiap elemen dalam form
                                    $namaPembeli = $data['nama_pembeli'];
                                    $totalbelanja = $data['total_harga'];
                                    $alamat = $data['alamat'];
                                    $metode = $data['metode'];


                                    $query = "INSERT INTO transaksi VALUES('', '$namaPembeli', '$totalbelanja', '$alamat', '$metode')";
                                    mysqli_query($conn, $query);
                                    return mysqli_affected_rows($conn);
                                } ?>
                            </tr>
                        </tfoot>
                    </table>
                    <form method="post">
                        <form action="" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="text" name="nama_pembeli" class="form-control" placeholder="Nama Pembeli">
                            </div>
                            <div class="form-group">
                                <input type="text" name="total_harga" class="form-control" placeholder="Yang harus dibayar : Rp<?php echo number_format($totalbelanja) ?>">
                            </div>
                            <div class="form-group">
                                <input type="text" name="alamat" class="form-control" placeholder="Alamat Tujuan">
                            </div>
                            <div class="form-label-group">
                                <select class="form-control" name="Metode Pembayaran">
                                    <option selected>Metode Pembayaran</option>
                                    <!-- <option value="kasir">Kasir</option> -->
                                    <option value="cod">COD</option>
                                    <option value="gopay">Gopay</option>
                                    <option value="ovo">OVO</option>
                                    <option value="dana">Dana</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <br>
                                <input type="submit" name="submit" value="Konfirmasi" class="btn btn-primary btn-block">
                            </div>
                        </form>
                    </form>
                </div>
            </div>
        </div>

    </section>




</body>

</html>