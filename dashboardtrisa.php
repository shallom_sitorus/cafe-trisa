<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>



<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
<div class="row mt-5 no-gutters">
    <div class="col-md-2 bg-light">
        <ul class="list-group list-group-flush p-2 pt-4">
            <li class="list-group-item bg-warning">MENU</li>
            <li class="list-group-item">Coffee</li>
            <li class="list-group-item">Hot Menu</li>
            <li class="list-group-item">Cold Menu</li>
            <li class="list-group-item">Dessert</li>
            <li class="list-group-item">Add Topping</li>
        </ul>
    </div>
    <div class="col-md-10">
        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="slide1.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Make Your Day Happy With Coffee</h5>
                        <p>Secangkir kopi akan membuat harimu lebih cerah dan menyenangkan.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="slide2.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Find Your Favorite Coffee</h5>
                        <p>Banyak rasa yang harus dirasakan, salah satunya rasa kopi favoritku.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="slide3.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>More espresso, Less despresso</h5>
                        <p>ringankan beban dengan minum kopi, dan nikmati.</p>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
        <h4 class="text-center font-weight-bold m-4">PRODUK PILIHAN</h4>
        <div class="row mx-auto">
            <div class="card col-md-3 mb-3">
                <img src="p1.jpg" class="card-img-top" alt="...">
                <div class="card-body bg-light">
                    <h5 class="card-title">Cold Coffee with frozen Coffee Ice Cubes</h5>
                    <i class="fas fa-star"></i>
                    <p class="card-text">Kopi belgium yang dipadukan dengan susu alami dan dicampur dengan es batu menambah rasa nikmat.</p>
                    <a href="#" class="btn btn-primary rounded-pill" data-bs-target="#produk1" data-bs-toggle="modal">Detail</a>
                    <a href="#" class="btn btn-success rounded-pill">Rp. 20.000</a>
                </div>
            </div>

            <div class="card col-md-3 mb-3">
                <img src="02.jpg" class="card-img-top" alt="...">
                <div class="card-body bg-light">
                    <h5 class="card-title">Dalgona Whipped Coffee</h5>
                    <i class="fas fa-star"></i>
                    <p class="card-text">Whipe cream dari kopiyang diaduk dengan sangat hati-hati dan cita rasa campuran susu dan kopi yang pas.</p>
                    <a href="#" class="btn btn-primary rounded-pill" data-bs-target="#produk2" data-bs-toggle="modal">Detail</a>
                    <a href="#" class="btn btn-success rounded-pill">Rp. 25.000</a>
                </div>
            </div>

            <div class="card col-md-3 mb-3">
                <img src="03.jpg" class="card-img-top" alt="...">
                <div class="card-body bg-light">
                    <h5 class="card-title">Frothy Whipped Coffee White Russian</h5>
                    <i class="fas fa-star"></i>
                    <p class="card-text">Kopi hitam yang dicampur dengan susu dan ditambahkan dengan wipe cream yang dibuat dari kopi.</p>
                    <a href="#" class="btn btn-primary rounded-pill" data-bs-target="#produk3" data-bs-toggle="modal">Detail</a>
                    <a href="#" class="btn btn-success rounded-pill">Rp. 35.000</a>
                </div>
            </div>

            <div class="card col-md-3 mb-3">
                <img src="04.jpg" class="card-img-top" alt="...">
                <div class="card-body bg-light">
                    <h5 class="card-title">Brew Coffee in a French Press</h5>
                    <i class="fas fa-star"></i>
                    <p class="card-text">Kopi hitam yang menawan karena dicampur dengan susu yang sudah diolah.</p>
                    <a href="#" class="btn btn-primary rounded-pill" data-bs-target="#produk4" data-bs-toggle="modal">Detail</a>
                    <a href="#" class="btn btn-success rounded-pill">Rp. 30.000</a>
                </div>
            </div>

        </div>

        <div class="row mx-auto mt-5 mb-3">
            <div class="card col-md-3">
                <img src="05.jpg" class="card-img-top" alt="...">
                <div class="card-body bg-light">
                    <h5 class="card-title">Kitchn</h5>
                    <i class="fas fa-star"></i>
                    <p class="card-text">Kopi dengan hiasan yang bisa di request sesuai kemauan anda.</p>
                    <a href="#" class="btn btn-primary rounded-pill" data-bs-target="#produk5" data-bs-toggle="modal">Detail</a>
                    <a href="#" class="btn btn-success rounded-pill">Rp. 15.000</a>
                </div>
            </div>

            <div class="card col-md-3 mb-3">
                <img src="06.jpg" class="card-img-top" alt="...">
                <div class="card-body bg-light">
                    <h5 class="card-title">Huzelnut Iced Coffee</h5>
                    <i class="fas fa-star"></i>
                    <p class="card-text">Minuman kopi dingin yang dicampur dengan susu dan rasa hazulnat yang memenuhi rasa.</p>
                    <a href="#" class="btn btn-primary rounded-pill" data-bs-target="#produk6" data-bs-toggle="modal">Detail</a>
                    <a href="#" class="btn btn-success rounded-pill">Rp. 30.000</a>
                </div>
            </div>

            <div class="card col-md-3 mb-3">
                <img src="07.jpg" class="card-img-top" alt="...">
                <div class="card-body bg-light">
                    <h5 class="card-title">Basic Coffee for Weight Loss</h5>
                    <i class="fas fa-star"></i>
                    <p class="card-text">Memiliki rasa yang enak tapi tetap menjaga bentuk ideal tubuh dan rasa diet bertambah.</p>
                    <a href="#" class="btn btn-primary rounded-pill" data-bs-target="#produk7" data-bs-toggle="modal">Detail</a>
                    <a href="#" class="btn btn-success rounded-pill">Rp. 30.000</a>
                </div>
            </div>


            <div class="card col-md-3 mb-3">
                <img src="08.jpg" class="card-img-top" alt="...">
                <div class="card-body bg-light">
                    <h5 class="card-title">Greek Coffee</h5>
                    <i class="fas fa-star"></i>
                    <p class="card-text">Kopi khas Turki yang sangat digemari karena rasa manis dari rasa cokelat menambah rasa kopi sangat enak.</p>
                    <a href="#" class="btn btn-primary rounded-pill" data-bs-target="#produk8" data-bs-toggle="modal">Detail</a>
                    <a href="#" class="btn btn-success rounded-pill">Rp. 25.000</a>
                </div>
            </div>

            <h4 class="text-center font-weight-bold m-4">DESSERT FOR YOU</h4>
            <div class="card col-md-3 mb-3">
                <img src="09.jpg" class="card-img-top" alt="...">
                <div class="card-body bg-light">
                    <h5 class="card-title">Dessert box</h5>
                    <i class="fas fa-star"></i>
                    <p class="card-text">Campuran dari kue, cream, Whipe, dan lumeran coklat akan membuat mood hari anda akan kembali.</p>
                    <a href="#" class="btn btn-primary rounded-pill" data-bs-target="#produk9" data-bs-toggle="modal">Detail</a>
                    <a href="#" class="btn btn-success rounded-pill">Rp. 40.000</a>
                </div>
            </div>

            <div class="card col-md-3 mb-3">
                <img src="10.jpg" class="card-img-top" alt="...">
                <div class="card-body bg-light">
                    <h5 class="card-title">California's Churros</h5>
                    <i class="fas fa-star"></i>
                    <p class="card-text">Churros yang cocok dibuat untuk cemilan, rasa yang gurih dan manis sangat pas untuk disantap bersama-sama.</p>
                    <a href="#" class="btn btn-primary rounded-pill" data-bs-target="#produk10" data-bs-toggle="modal">Detail</a>
                    <a href="#" class="btn btn-success rounded-pill">Rp. 25.000</a>
                </div>
            </div>

            <div class="card col-md-3 mb-3">
                <img src="11.jpg" class="card-img-top" alt="...">
                <div class="card-body bg-light">
                    <h5 class="card-title">Crepe Cakes</h5>
                    <i class="fas fa-star"></i>
                    <p class="card-text">Satu demi satu bagain slice dari kue disusun hingga membentuk kue yang sangat pas untuk menemani hari anda.</p>
                    <a href="#" class="btn btn-primary rounded-pill" data-bs-target="#produk11" data-bs-toggle="modal">Detail</a>
                    <a href="#" class="btn btn-success rounded-pill">Rp. 30.000</a>
                </div>
            </div>

            <div class="card col-md-3 mb-3">
                <img src="12.jpg" class="card-img-top" alt="...">
                <div class="card-body bg-light">
                    <h5 class="card-title">Pancake with milk</h5>
                    <i class="fas fa-star"></i>
                    <p class="card-text">Kelembutan dari roti yang digoreng ini sangat pas dan cocok untuk disandingkan dengan kopi.</p>
                    <a href="#" class="btn btn-primary rounded-pill" data-bs-target="#produk12" data-bs-toggle="modal">Detail</a>
                    <a href="#" class="btn btn-success rounded-pill">Rp. 45.000</a>
                </div>
            </div>
        </div>
        <div class="modal fade" id="produk1" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Cold Coffee with frozen Coffee Ice Cubes</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <img src="p1.jpg">
                            </div>
                            <div class="mb-3">
                                <h6>Deskripsi</h6>
                                <a>Kopi belgium yang dipadukan dengan susu alami dan dicampur dengan es batu menambah rasa nikmat.</a>
                                <h6>Price</h6>
                                <a>Rp. 20.000</a>
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Notes</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Kembali</button>
                        <a href="datatables/index.php" class="btn btn-outline-success" role="button" aria-pressed="true">Pesan</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="produk2" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Dalgona Whipped Coffee</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <img src="02.jpg">
                            </div>
                            <div class="mb-3">
                                <h6>Deskripsi</h6>
                                <a>Whipe cream dari kopiyang diaduk dengan sangat hati-hati dan cita rasa campuran susu dan kopi yang pas.</a>
                                <h6>Price</h6>
                                <a>Rp. 25.000</a>
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Notes</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Kembali</button>
                        <a href="datatables/index.php" class="btn btn-outline-success" role="button" aria-pressed="true">Pesan</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="produk3" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Frothy Whipped Coffee White Russian</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <img src="03.jpg">
                            </div>
                            <div class="mb-3">
                                <h6>Deskripsi</h6>
                                <a>Kopi hitam yang dicampur dengan susu dan ditambahkan dengan wipe cream yang dibuat dari kopi.</a>
                                <h6>Price</h6>
                                <a>Rp. 35.000</a>
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Notes</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Kembali</button>
                        <a href="datatables/index.php" class="btn btn-outline-success" role="button" aria-pressed="true">Pesan</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="produk4" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Brew Coffee in a French Press</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <img src="04.jpg">
                            </div>
                            <div class="mb-3">
                                <h6>Deskripsi</h6>
                                <a>Kopi hitam yang menawan karena dicampur dengan susu yang sudah diolah.</a>
                                <h6>Price</h6>
                                <a>Rp. 30.000</a>
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Notes</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Kembali</button>
                        <a href="datatables/index.php" class="btn btn-outline-success" role="button" aria-pressed="true">Pesan</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="produk5" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Kitchn</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <img src="05.jpg">
                            </div>
                            <div class="mb-3">
                                <h6>Deskripsi</h6>
                                <a>Kopi dengan hiasan yang bisa di request sesuai kemauan anda.</a>
                                <h6>Price</h6>
                                <a>Rp. 15.000</a>
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Notes</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Kembali</button>
                        <a href="datatables/index.php" class="btn btn-outline-success" role="button" aria-pressed="true">Pesan</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="produk6" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Huzelnut Iced Coffee</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <img src="06.jpg">
                            </div>
                            <div class="mb-3">
                                <h6>Deskripsi</h6>
                                <a>Minuman kopi dingin yang dicampur dengan susu dan rasa hazulnat yang memenuhi rasa.</a>
                                <h6>Price</h6>
                                <a>Rp. 30.000</a>
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Notes</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Kembali</button>
                        <a href="datatables/index.php" class="btn btn-outline-success" role="button" aria-pressed="true">Pesan</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="produk7" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Basic Coffee for Weight Loss</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <img src="07.jpg">
                            </div>
                            <div class="mb-3">
                                <h6>Deskripsi</h6>
                                <a>Memiliki rasa yang enak tapi tetap menjaga bentuk ideal tubuh dan rasa diet bertambah.</a>
                                <h6>Price</h6>
                                <a>Rp. 30.000</a>
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Notes</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Kembali</button>
                        <a href="datatables/index.php" class="btn btn-outline-success" role="button" aria-pressed="true">Pesan</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="produk8" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Greek Coffee</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <img src="08.jpg">
                            </div>
                            <div class="mb-3">
                                <h6>Deskripsi</h6>
                                <a>Kopi khas Turki yang sangat digemari karena rasa manis dari rasa cokelat menambah rasa kopi sangat enak.</a>
                                <h6>Price</h6>
                                <a>Rp. 25.000</a>
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Notes</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Kembali</button>
                        <a href="datatables/index.php" class="btn btn-outline-success" role="button" aria-pressed="true">Pesan</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="produk9" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Dessert Box</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <img src="09.jpg">
                            </div>
                            <div class="mb-3">
                                <h6>Deskripsi</h6>
                                <a>Campuran dari kue, cream, Whipe, dan lumeran coklat akan membuat mood hari anda akan kembali.</a>
                                <h6>Price</h6>
                                <a>Rp. 40.000</a>
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Notes</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Kembali</button>
                        <a href="datatables/index.php" class="btn btn-outline-success" role="button" aria-pressed="true">Pesan</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="produk10" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">California's Churros</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <img src="10.jpg">
                            </div>
                            <div class="mb-3">
                                <h6>Deskripsi</h6>
                                <a>Churros yang cocok dibuat untuk cemilan, rasa yang gurih dan manis sangat pas untuk disantap bersama-sama.</a>
                                <h6>Price</h6>
                                <a>Rp. 25.000</a>
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Notes</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Kembali</button>
                        <a href="datatables/index.php" class="btn btn-outline-success" role="button" aria-pressed="true">Pesan</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="produk11" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Crepe Cakes</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <img src="11.jpg">
                            </div>
                            <div class="mb-3">
                                <h6>Deskripsi</h6>
                                <a>Satu demi satu bagain slice dari kue disusun hingga membentuk kue yang sangat pas untuk menemani hari anda.</a>
                                <h6>Price</h6>
                                <a>Rp. 30.000</a>
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Notes</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Kembali</button>
                        <a href="datatables/index.php" class="btn btn-outline-success" role="button" aria-pressed="true">Pesan</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="produk12" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Pancake with Milk</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <img src="12.jpg">
                            </div>
                            <div class="mb-3">
                                <h6>Deskripsi</h6>
                                <a>Kelembutan dari roti yang digoreng ini sangat pas dan cocok untuk disandingkan dengan kopi.</a>
                                <h6>Price</h6>
                                <a>Rp. 45.000</a>
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Notes</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Kembali</button>
                        <a href="datatables/index.php" class="btn btn-outline-success" role="button" aria-pressed="true">Pesan</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- <section id="contact">
            <div class="container">
                <div class="row text-center">
                    <div class="col">
                        <h2>Contact Me</h2>
                    </div>
                </div>
            </div>
        </section> -->
    </div>
</div>

<footer class="text-center text-white" style="background-color: #caced1;">
    <!-- Grid container -->
    <div class="container p-4">
        <!-- Section: Images -->
        <section class="">
            <div class="row">
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div class="bg-image hover-overlay ripple shadow-1-strong rounded" data-ripple-color="light">
                        <img src="cafe6.jpg" class="w-100" />
                        <a href="#!">
                            <div class="mask" style="background-color: rgba(251, 251, 251, 0.2);"></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div class="bg-image hover-overlay ripple shadow-1-strong rounded" data-ripple-color="light">
                        <img src="cafe5.jpg" class="w-100" />
                        <a href="#!">
                            <div class="mask" style="background-color: rgba(251, 251, 251, 0.2);"></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div class="bg-image hover-overlay ripple shadow-1-strong rounded" data-ripple-color="light">
                        <img src="cafe4.jpg" class="w-100" />
                        <a href="#!">
                            <div class="mask" style="background-color: rgba(251, 251, 251, 0.2);"></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div class="bg-image hover-overlay ripple shadow-1-strong rounded" data-ripple-color="light">
                        <img src="cafe3.jpg" class="w-100" />
                        <a href="#!">
                            <div class="mask" style="background-color: rgba(251, 251, 251, 0.2);"></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div class="bg-image hover-overlay ripple shadow-1-strong rounded" data-ripple-color="light">
                        <img src="cafe2.jpg" class="w-100" />
                        <a href="#!">
                            <div class="mask" style="background-color: rgba(251, 251, 251, 0.2);"></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div class="bg-image hover-overlay ripple shadow-1-strong rounded" data-ripple-color="light">
                        <img src="cafe1.jpg" class="w-100" />
                        <a href="#!">
                            <div class="mask" style="background-color: rgba(251, 251, 251, 0.2);"></div>
                        </a>
                    </div>
                </div>
                <!-- Grid container -->
                <div class="container p-4">
                    <!--Grid row-->
                    <div class="row">
                        <!--Grid column-->
                        <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
                            <h5 class="text-uppercase">Life Happens, Coffee Helps</h5>

                            <p>
                                Apapun masalah yang kamu hadapi, kopi akan selalu menemani dan selalu ada untuk membuatmu happy. Keep smile and do your best!
                                There is always time Coffee
                            </p>
                        </div>
                        <!--Grid column-->

                        <!--Grid column-->
                        <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
                            <h5 class="text-uppercase">Coffe is strong and so you are</h5>

                            <p>
                                Kamu tahu apa yang kamu lakukan adalah masalah besar dan kamu akan pusing jika menghadapi sendiri, maka kamu harus ditemani dengan kopi!
                            </p>
                        </div>
                        <!--Grid column-->
                    </div>
                    <!--Grid row-->
                </div>
</footer>
</div>
</section>
<!-- Section: Images -->
</div>
<!-- Grid container -->
</footer>
</body>

</html>