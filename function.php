<?php

$conn = mysqli_connect('localhost', 'root', '', 'cafe_trisa');



function query($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }

    return $rows;
}

function daftar($data)
{

    global $conn;
    $username = strtolower($data["username"]);
    $email = $data["email"];
    $password = mysqli_escape_string($conn, $data["password"]);
    $password2 = mysqli_escape_string($conn, $data["password2"]);
    $level = strtolower(stripslashes($data["level"]));

    //cek username dan email sdh terdaftar
    $result = mysqli_query($conn, "SELECT username FROM user
                            WHERE username = '$username'");


    if (mysqli_fetch_assoc($result)) {
        echo "
        <script>
            alert('username sudah terdaftar');
        </script>
        ";
        return false;
    }

    $emaill = mysqli_query($conn, "SELECT email FROM user
                            WHERE email = '$email'");


    if (mysqli_fetch_assoc($emaill)) {
        echo "
        <script>
            alert('email sudah terdaftar');
        </script>
        ";
        return false;
    }

    //cek pw tdk sama
    if ($password != $password2) {
        echo "
        <script>
            alert('konfirmasi password tidak sama');
        </script>
        ";
        return false;
    } else {
        $password = password_hash($password, PASSWORD_DEFAULT);
        mysqli_query($conn, "INSERT INTO user VALUES ('', '$username', '$email', '$password', '$level')");
        return mysqli_affected_rows($conn);
    }
}

function tambah($data)
{

    global $conn;
    //ambil data dari tiap elemen dalam form
    $nama_lengkap = $data['nama_lengkap']; 
    $no_hp = $data['no_hp'];
    $tanggal_lahir = $data['tanggal_lahir'];
    $alamat = $data['alamat'];
    $jenis_kelamin = $data['jenis_kelamin'];

    $foto = upload();
    if (!$foto) {
        return false;
    }

    $query = "INSERT INTO member VALUES
    ('', '$nama_lengkap', '$no_hp', '$tanggal_lahir', '$alamat', '$jenis_kelamin', '$foto')
";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function upload()
{

    $namaFile = $_FILES['foto']['name'];
    $ukuranFile = $_FILES['foto']['size'];
    $error = $_FILES['foto']['error'];
    $tmpName = $_FILES['foto']['tmp_name'];


    if ($error === 4) {
        echo "<script>
        alert('Pilih gambar terlebih dahulu')
        </script>";
        return false;
    }

    $ekstensiGambarValid = ['jpg', 'jpeg', 'png', 'gif'];
    $ekstensiGambar = explode('.', $namaFile);
    $ekstensiGambar = strtolower(end($ekstensiGambar));

    if (!in_array($ekstensiGambar, $ekstensiGambarValid)) {
        echo "<script>
        alert('yang anda upload bukan gambar');
        </script>";
        return false;
    }
    //cek jika ukuran terlalu besar
    if ($ukuranFile > 1000000) {
        echo "<script>
            alert('ukuran gambar anda terlalu besar');
            </script>
            ";
        return false;
    }

    $namaFileBaru = uniqid();
    $namaFileBaru .= '.';
    $namaFileBaru .= $ekstensiGambar;

    move_uploaded_file($tmpName, 'img/' . $namaFileBaru);

    return ($namaFileBaru);
}

function member($keyword)
{
    $query = "SELECT * FROM member WHERE nama_lengkap LIKE
    '%$keyword%'
    OR no_hp LIKE '%$keyword%'
    OR tanggal_lahir LIKE '%$keyword%'
    OR alamat LIKE '%$keyword%'
    OR jenis_kelamin LIKE '%$keyword%'
   ";
    return query($query);
}

function ubahmember($data)
{
    global $conn;
    $id = $data["id"];
    $nama_lengkap = $data["nama_lengkap"];
    $no_hp = $data["no_hp"];
    $tanggal_lahir = $data["tanggal_lahir"];
    $alamat = $data["alamat"];
    $jenis_kelamin = $data["jenis_kelamin"];
    $gambarlama = $data["gambarlama"];

    //CEK apakah user pilih gambar baru atau tidak
    if ($_FILES['foto']['error'] === 4) {
        $foto = $gambarlama;
    } else {
        $foto = upload();
    }
    //query insert data
    $query = "UPDATE member SET
nama_lengkap = '$nama_lengkap',
no_hp = '$no_hp',
tanggal_lahir = '$tanggal_lahir',
alamat = '$alamat',
jenis_kelamin = '$jenis_kelamin',
foto = '$foto'
WHERE id_member = '$id'
";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function hapusmember($id)
{

    global $conn;
    $query = "DELETE FROM member WHERE id_member=$id";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}


function tambahmenu($data)
{

    global $conn;
    $id_menu = $data['id_menu'];
    $nama_menu = $data['nama_menu'];
    $harga = $data['harga'];
    $stok= $data['stok'];
    $kategori = strtolower(stripslashes($data["kategori"]));
    $deskripsi = $data['deskripsi'];

    $gambar = uploadmenu();
    if (!$gambar) {
        return false;
    }

    //query insert data
    $query = "INSERT INTO menu VALUES
    ('$id_menu', '$nama_menu', '$harga', '$stok', '$kategori', '$deskripsi', '$gambar')
";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function uploadmenu()
{

    $namaFile = $_FILES['gambar']['name'];
    $ukuranFile = $_FILES['gambar']['size'];
    $error = $_FILES['gambar']['error'];
    $tmpName = $_FILES['gambar']['tmp_name'];

    if ($error === 4) {
        echo "<script>
        alert('Pilih gambar terlebih dahulu')
        </script>";
        return false;
    }

    $ekstensiGambarValid = ['jpg', 'jpeg', 'png', 'gif'];
    $ekstensiGambar = explode('.', $namaFile);
    $ekstensiGambar = strtolower(end($ekstensiGambar));

    if (!in_array($ekstensiGambar, $ekstensiGambarValid)) {
        echo "<script>
        alert('yang anda upload bukan gambar');
        </script>";
        return false;
    }

    if ($ukuranFile > 1000000) {
        echo "<script>
            alert('ukuran gambar anda terlalu besar');
            </script>
            ";
        return false;
    }

    $namaFileBaru = uniqid();
    $namaFileBaru .= '.';
    $namaFileBaru .= $ekstensiGambar;

    move_uploaded_file($tmpName, 'img/' . $namaFileBaru);

    return ($namaFileBaru);

}

function menu($keyword)
{
    $query = "SELECT * FROM menu WHERE nama_menu LIKE
    '%$keyword%'
    OR harga LIKE '%$keyword%'
    OR stok LIKE '%$keyword%'
    OR kategori LIKE '%$keyword%'
   ";
    return query($query);
}

function ubahmenu($data)
{
    global $conn;
    $id = $data["id"];
    $nama_menu = $data["nama_menu"];
    $harga = $data["harga"];
    $stok = $data["stok"];
    $kategori = $data["kategori"];
    $deskripsi = $data['deskripsi'];
    $gambarlama = $data["gambarlama"];

    if ($_FILES['gambar']['error'] === 4) {
        $gambar = $gambarlama;
    } else {
        $gambar = uploadmenu();
    }
    $query = "UPDATE menu SET
    nama_menu = '$nama_menu',
    harga = '$harga',
    stok = '$stok',
    kategori = '$kategori',
    deksripsi = '$deskripsi',
    gambar = '$gambar'
    WHERE id_menu = '$id'
    ";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);

}

function hapusmenu($id)
{

    global $conn;
    $query = "DELETE FROM menu WHERE id_menu=$id";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

// function transaksi_lanjut($data)
// {

//     global $conn;
//     //ambil data dari tiap elemen dalam form
//     $namaPembeli = $data['nama_pembeli'];
//     $jumlah = $data['total_harga'];
//     $alamat = $data['alamat'];
//     $metode = $data['metode'];
    

//     $query = "INSERT INTO transaksi VALUES('', '$namaPembeli', '$jumlah', '$alamat', '$metode')";
//     mysqli_query($conn, $query);
//     return mysqli_affected_rows($conn);
// }

function daftarpembelian($keyword)
{
    $query = "SELECT * FROM transaksi WHERE nama_pembeli LIKE
    '%$keyword%'
    OR total_harga LIKE '%$keyword%'
    OR alamat LIKE '%$keyword%'
    OR metode LIKE '%$keyword%'
   ";
    return query($query);
}