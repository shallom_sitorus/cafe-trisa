<?php

require 'function.php';

if (isset($_POST["submit"])) {

    if (tambah($_POST) > 0) {
        echo "
        <script>
            alert('Data BERHASIL ditambahkan');
            document.location.href = 'dashboard_admin.php';
        </script>
        ";
    } else {
        echo "
        <script>
        alert('Data GAGAL ditambahkan');
        document.location.href = 'dashboard_admin.php';
        </script>

    ";
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DASHBOARD ADMIN</title>
  <link rel="stylesheet" href="styleloginsignup.css">
</head>

<body>
  <div class="container">
    <h1>Tambah Data User</h1>
    <form action="" method="post">
      <label>Nama Lengkap</label>
      <br>
      <input name="nama_lengkap" type="text" require>
      <br>
      <label>Username</label>
      <br>
      <input name="username" type="text" require>
      <br>
      <label>Email</label>
      <br>
      <input name="email" type="text" require>
      <br>
      <label>Password</label>
      <br>
      <input name="password" type="password" require>
      <br>
      <br>
      <label>Konfirmasi Password</label>
      <br>
      <input name="password2" type="password" require>
      <br>
      <button type="submit" name="submit">Daftar</button>
    </form>
  </div>
</body>
</html>

<!-- <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DASHBOARD ADMIN</title>
</head>

<body>
    <h1>Form Tambah User</h1>
    <form action="" method="post">
        <table>
            <tr>
                <td>NIM</td>
                <td><input type="text" name="nim" id="nim"></td>
            </tr>
            <tr>
                <td>Nama</td>
                <td><input type="text" name="nama" id="nama"></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><input type="text" name="email" id="email"></td>
            </tr>
            <tr>
                <td>Jurusan</td>
                <td><input type="text" name="jurusan" id="jurusan"></td>
            </tr>
            <tr>
                <td></td>
                <td><button type="submit" name="submit">Simpan</button></td>
            </tr>
        </table>
    </form>
</body>

</html> -->
